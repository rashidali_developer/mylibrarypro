from odoo import http
from openerp.http import Controller, route, request
import base64
import os
# from flask import Flask, jsonify
import json


class WebsiteForm(http.Controller):
    @http.route(['/appointment'], type='http', auth="user", website=True)
    def appointment(self, **kw):
        publisher = request.env['library.book'].sudo().search([])
        publisher = request.env['res.book'].sudo().search([])
        partner_id = request.env.user.partner_id
        Attachment = request.env['ir.attachment']
        file_name = post.get('attachment').filename
        file = post.get('attachment')
        attachment_id = Attachment.create({
            'name': file_name,
            'type': 'binary',
            'datas': base64.b64encode(file.read()),
            'res_model': partner_id._name,
            'res_id': partner_id.id
        })
        values = {}
        values.update({
            'publisher': publisher,
            'attachment': [(4, attachment_id.id)],
        })
        return http.request.render("my_library.online_appointment_form", values)

    @http.route(['/appointment/submit/'], type='http', auth="user", website=True)
    def submit(self, **kw):
        print('received record -------------------->', kw)
        request.env['library.book'].sudo().create(kw)
        return request.render("my_library.submit", {})

    # @route(['/my/account'], type='http', auth='public', website=True)
    # def file_upload(self, redirect=None, **post):
    #     partner_id = request.env.user.partner_id
    #     Attachment = request.env['ir.attachment']
    #     file_name = post.get('attachment').filename
    #     file = post.get('attachment')
    #     attachment_id = Attachment.create({
    #         'name': file_name,
    #         'type': 'binary',
    #         'datas': base64.b64encode(file.read()),
    #         'res_model': partner_id._name,
    #         'res_id': partner_id.id
    #     })
    #     partner_id.update({
    #         'attachment': [(4, attachment_id.id)],
    #     })


    @http.route('/create/user/', type='http', auth='public', website=True)
    def create_usr(self):
        child_value = {
            'name':'Rashid Alicvzvzx'
        }
        parent_value = {
            'name':'Rashid Alivxcvxv',
            'child_ids':[
                (0,0,child_value)
            ]
        }
        record  = http.request.env['res.partner'].create(parent_value)
        return record


    @http.route('/my_library/books/json', type='http' ,auth='public',website=True)
    def books_json(self):
        records = request.env['res.partner'].sudo().search([])
        data = []
        for record in records:
            data.append({
                'id': record.id,
                'name': record.name,
                'email': record.email,
            })
        response = json.dumps(data)
        return http.Response(
            response,
            content_type='application/json',
        )

    @http.route('/my_library/api/', type='json', website=False, method=['GET','POST'],csrf=False,auth='public')
    def rest_api(self):
        return 'Hello World'
