from odoo import http
from openerp.http import Controller, route, request

class graph_controller(http.Controller):
    @http.route('/library/book/', auth='none', type='http',method=['GET'])
    def check_method_get(self):
        record_library = request.env['library.book'].search([])
        for rec in record_library:
            return (rec.name)
