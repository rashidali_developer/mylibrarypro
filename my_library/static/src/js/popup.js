odoo.define('my_library.odoo_tutorial', function (require) {
    'use strict';

    var FormController = require('web.FormController');
    var ExtendFormController = FormController.include({
        saveRecord: function () {
            var res = this._super.apply(this, arguments);

            if (this.modelName == 'library.book') {
                this.do_notify('Success', 'Record Saved');
                var url = window.location.href;
                var idMatch = url.match(/[?&#]id=([^&]+)/);
                var activeId = parseInt(idMatch[1]);
                var domain = [['id', '=', activeId]];
                this._rpc({
                    model: 'library.book',
                    method: 'search_read',
                    fields: ['name','publisher_id'],
                    domain: domain,
                }).then(function (result) {
                    console.log('Search result:', result);
                });
            }
            return res;
        }
    });
});
