from odoo import models, fields, api
from odoo.exceptions import ValidationError
from datetime import datetime, timedelta


class BookCategory(models.Model):
    _name = 'library.book.category'

    name = fields.Char('Category')
    parent_id = fields.Many2one('library.book.category', string='Parent Category', ondelete='restrict', index=True)
    child_ids = fields.One2many('library.book', 'category_id', string='Child Categories')
    _parent_store = True
    _parent_name = "parent_id"  # optional if field is 'parent_id'
    parent_path = fields.Char(index=True)

    def create_categories(self):
        categ1 = {
            'name': 'Child____category 3',
        }
        categ2 = {
            'name': 'Child____c category 4',
        }
        parent_category_val = {
            'name': 'Parent____category 2',
            'parent_path': 'Parent____path 1',

        'child_ids': [
                (0, 0, categ1),
                (0, 0, categ2),
            ]
        }
        record = self.env['library.book.category'].create(parent_category_val)






    # @api.constraints('parent_id')
    # def _check_hierarchy(self):
    #     if not self._check_recursion():
    #         raise models.ValidationError('Error! You cannot create recursive categories.')
