from odoo import api, models, fields
from odoo.exceptions import ValidationError
from datetime import datetime, timedelta
from odoo.exceptions import UserError
from odoo.tools.translate import _


class LibraryBook(models.Model):
    # classical inheritance

    _name = "library.book"
    _description = 'Library Book'
    _order = 'date_release desc, name'
    _rec_name = 'short_name'

    name = fields.Char('Title')
    test = fields.Char('Test')
    short_name = fields.Char('Short Title')
    date_release = fields.Date('Release Date')
    author_ids = fields.Many2many('res.partner',
                                  string='Authors')
    manager_remarks = fields.Text('Manager Remarks')
    notes = fields.Text('Internal Notes')
    isbn = fields.Char('ISBN')
    state = fields.Selection(
        [('draft', 'Not Available'),
         ('available', 'Available'),
         ('borrowed', 'Borrowed'),
         ('lost', 'Lost')],
        'State', default="draft")

    states = fields.Selection(
        [('draft', 'Not Available'),
         ('available', 'Available'),
         ('borrowed', 'Borrowed'),
         ('lost', 'Lost')],
        'State', default="draft")
    description = fields.Html('Description')
    cover = fields.Binary('Book Cover')
    out_of_print = fields.Boolean('Out of Print?')
    date_updated = fields.Datetime('Last Updated')
    pages = fields.Integer('Number of Pages')
    reader_rating = fields.Float(
        'R.A.R',
        digits='Book Price',  # Optional precision decimals,
    )
    category_id = fields.Many2one('library.book.category')

    currency_id = fields.Many2one('res.currency', string='Currency')

    retail_price = fields.Monetary('Retail Price',
                                   currency_field='currency_id'
                                   )
    publisher_id = fields.Many2one(
        'res.partner', string='Publisher',
        # optional:
        ondelete='set null',
        context={},
        domain=[],
    )
    publisher_city = fields.Char(
        'Publisher City',
        related='publisher_id.city',
        readonly=True)

    age_days = fields.Float(
        string='Days Since Release',
        compute='_compute_age',
        inverse='_inverse_age',
        search='_search_age',
        store=False,
        # optional
        compute_sudo=True  # optional
    )

    ref_doc_id = fields.Reference(
        selection='_referencable_models',
        string='Reference Document')

    student_name = fields.Char(string='Name of the student')

    def without_authord_ids(self):
        records = self.env['library.book'].search([('states', '=', 'available'), ('date_release', '=', fields.Date.today())])
        for record in records:
                new_record = record.copy(default={'student_name': record.student_name + ' (copy cron job)'})


    _sql_constraints = [
        ('name_uniq', 'UNIQUE (name)', 'Book title must be unique.'),
        ('positive_page', 'CHECK(pages>0)', 'No of pages must be positive')
    ]

    @api.onchange('date_release')
    def _check_release_date(self):
        for record in self:
            if record.date_release and record.date_release > fields.Date.today():
                raise models.ValidationError('Release date must be in the past')

    @api.depends('date_release')
    def _compute_age(self):
        # ....
        today = fields.Date.today()
        for book in self:
            if book.date_release:
                delta = today - book.date_release
                book.age_days = delta.days
            else:
                book.age_days = 0

    def _inverse_age(self):
        # ......
        today = fields.Date.today()
        for book in self.filtered('date_release'):
            d = today - timedelta(days=book.age_days)
            book.date_release = d

    def _search_age(self, operator, value):
        today = fields.Date.today()
        value_days = timedelta(days=value)
        value_date = today - value_days
        # convert the operator:
        # book with age > value have a date < value_date
        operator_map = {
            '>': '<', '>=': '<=',
            '<': '>', '<=': '>=',
        }
        new_op = operator_map.get(operator, operator)
        return [('date_release', new_op, value_date)]

    @api.model
    def _referencable_models(self):
        models = self.env['ir.model'].search([('field_id.name', '=', 'message_ids')])
        return [(x.model, x.name) for x in models]

    # >>>>>>>>>>>>>>>>model methods and using api decorators on button click call functions Chapter 5>>>>>>>>>>>>>>>>>>>>>>>

    @api.model
    def is_allowed_transition(self, old_state, new_state):
        allowed = [('draft', 'available'),
                   ('available', 'borrowed'),
                   ('borrowed', 'available'),
                   ('available', 'lost'),
                   ('borrowed', 'lost'),
                   ('lost', 'available')]
        return (old_state, new_state) in allowed

    def change_state(self, new_state):
        for book in self:
            if book.is_allowed_transition(book.states, new_state):
                book.states = new_state
            else:
                msg = _('Moving from %s to %s is not allowed') % (book.states, new_state)
                raise UserError(msg)

    def make_available(self):
        self.change_state('available')

    def make_borrowed(self):
        self.change_state('borrowed')

    # def make_lost(self):
    #     self.change_state('lost')
    def make_lost(self):
        self.ensure_one()
        self.state = 'lost'
        if not self.env.context.get('avoid_deactivate'):
            self.active = False

    # >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

    # >>>>>>>>>>>>>>>>Recordset for different model Chapter 5 >>>>>>>>>>>>

    def log_all_library_members(self):
        library_member_model = self.env['library.book'].search([])
        print("ALL Books:", library_member_model)

    # >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

    def on_click_new_record(self):
        data = {
            'name': 'Farooq',
            'short_name': 'Kashmiri',
        }
        recordset = self.env['library.book'].create(data)

    def change_release_date(self):
        self.ensure_one()
        self.date_release = fields.Date.today()

    def change_update_data(self):
        self.ensure_one()
        self.update({
            'date_release': fields.Datetime.now(),
            'name': 'Farooqqqqq',
            'short_name': 'Kashmiri',
        })

    def find_book(self):
        domain = [
            '|', '&', ('name', 'ilike', 'Book Name'),
            ('category_id.name', 'ilike', 'Category Name'),
            '&', ('name', 'ilike', 'Book Name 2'),
            ('category_id.name', 'ilike', 'Category Name 2')]
        books = self.search(domain)
        print(books.name)

    def find_partner(self):
        PartnerObj = self.env['res.partner']
        domain = [
            '&', ('name', 'ilike', 'Parth Gajjar'),
            ('company_id.name', '=', 'Odoo')
        ]
        partner = PartnerObj.search(domain)

    def test_record(self):
        for rec in self:
            print("odoo ORM : Record Set Operation")
            partners = self.env['library.book'].search([]).browse([2, 3])
            for partner in partners:
                name = partner.name
                for author in partner.author_ids:
                    author = author.name
                    print(name, author)

            partners = self.env['library.book'].search([]).filtered(lambda r: r.name)
            print(partners)

            # partners1 = self.env['library.book'].search([],limit=1)
            # print("Mapped Partner........", partners.mapped(lambda x:x.name))
            # print("Reverse Sorted Partner........", partners1.sorted(lambda o: o.id, reverse=True))
            # print("Sorted Partner........", partners.sorted(lambda o: o.write_date))
            # print(" Filtered Partner........", partners.filtered(lambda o: o.publisher_id.name))

    def author_records(self):
        for rec in self:
            print("odoo ORM : Mapped")
            partners = self.env['res.partner'].search([])
            print("Author Names........", partners.mapped('name'))

    def name_get(self):
        result = []
        for book in self:
            authors = book.author_ids.mapped('name')
            name = '%s (%s)' % (book.name, ', '.join(authors))
            result.append((book.id, name))
        return result

    def book_rent(self):
        self.ensure_one()
        if self.states != 'available':
            raise UserError(_('Book is not available for renting'))
        rent_as_superuser = self.env['library.book.rent'].sudo()
        rent_as_superuser.create({
            'book_id': self.id,
            'borrower_id': self.env.user.partner_id.id,
        })

    #  executing raw sql queries 8 chapter
    def average_book_occupation(self):
        self.flush()
        sql_query = "select * From library_book as lb WHERE lb.name = 'Rashid Ali';"
        self.env.cr.execute(sql_query)
        result = self.env.cr.fetchall()
        print(result)
        # logger.info("Average book occupation: %s", result)


class ResPartner(models.Model):
    # extension inheritance
    _inherit = 'res.partner'
    _order = 'name'
    authored_book_ids = fields.Many2many('library.book', string='Authored Books')
    count_books = fields.Integer('Number of Authored Books', compute='_compute_count_books')

    @api.depends('authored_book_ids')
    def _compute_count_books(self):
        for r in self:
            r.count_books = len(r.authored_book_ids)


class LibraryMember(models.Model):
    _name = 'library.member'
    # Delegation Inheritance
    _inherits = {'res.partner': 'partner_id'}

    partner_id = fields.Many2one('res.partner', ondelete='cascade')
    date_start = fields.Date('Member Since')
    date_end = fields.Date('Termination Date')
    member_number = fields.Char()
    date_of_birth = fields.Date('Date of birth')

class Attachment(models.Model):
    _inherit = 'ir.attachment'
    attach_rel = fields.Many2many('res.partner', 'attachment', 'attachment_id', 'document_id', string = "Attachment")

class ResPartner(models.Model):
    _inherit='res.partner'
    attachment=fields.Many2many('ir.attachment', 'attach_rel', 'doc_id', 'attach_id', string="Attachment", help='You can upload your document', copy=False)