from odoo import models, fields, api
from datetime import datetime, timedelta


class LibraryBookCopy(models.Model):

    _name = "library.book.copy"
    _inherit = "library.book"
    _description = "Library Book's Copy"
    _inherits = {'res.partner':'partner_id'}


