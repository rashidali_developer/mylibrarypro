# -*- coding: utf-8 -*-
{
    'name': 'Library Returns',
    'version': '15.0.0',
    'summary': 'Library Retrun Book Store',
    'sequence': -100,
    'description': """Library Return Book Store""",
    'category': 'Productivity',
    'author': 'CookBook',
    'maintainer': 'CookBook',
    'website': 'https://www.Learnology.tech',
    'license': 'AGPL-3',
    'depends': ['base', 'mail'],
    'data': [
        'security/ir.model.access.csv',
        'views/library_book_return.xml',

    ],
    'demo': [],
    'qweb': [],
    # 'images': ['static/description/banner.gif'],
    'installable': True,
    'application': True,
    'auto_install': False,
}
