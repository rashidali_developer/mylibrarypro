from odoo import models, fields, api
from datetime import datetime, timedelta


class LibraryBookReturn(models.Model):
    _name = "library.book.return"
    _description = 'Library Return Book'

    name = fields.Char('Name')
    email = fields.Char('Email')
    phone = fields.Char('Phone')
    address = fields.Char('Address')

# extension inheritance when we add fields and methods in an exsiting model is called extension inheritance
class LibraryBook(models.Model):
    _inherit = 'library.book'
    date_return = fields.Date('Date to return')


    def make_borrowed(self):
        day_to_borrow = self.category_id.max_borrow_days or 10
        self.date_return = fields.Date.today() + timedelta(days=day_to_borrow)
        return super(LibraryBook, self).make_borrowed()
    #

    def make_available(self):
        self.date_return = False
        return super(LibraryBook, self).make_available()


    # create method is used to create the new records in a model and super method is tregered when override the parent class features or properties and methods in a child class.
    @api.model
    def create(self, values):
        if not self.user_has_groups('my_library.acl_book_librarian'):
            if 'manager_remarks' in values:
                raise UserError('You are not allowed to modify ''manager_remarks')
        return super(LibraryBook, self).create(values)


    # write method is tregered when any record is updated and super method is used to overide the parent class features and properties
    def write(self, values):
        if not self.user_has_groups('my_library.group_librarian'):
            if 'manager_remarks' in values:
                del values['manager_remarks']
            return super(LibraryBook, self).write(values)


    # unlink is tegered when id is deleted and super class understand this method is override.
    def unlink(self):
        return super(LibraryBook, self).unlink()


    # _name_search method is tregered and it search name , isbn and author name if any field is present then declare as a title
    @api.model
    def _name_search(self, name='', args=None, operator='ilike', limit=100, name_get_uid=None):
        args = [] if args is None else args.copy()
        if not (name == '' and operator == 'ilike'):
            args += ['|', '|', ('name', operator, name), ('isbn', operator, name), ('author_ids.name', operator, name)]
            return super(LibraryBook, self)._name_search(name=name, args=args, operator=operator,
                                                         limit=limit, name_get_uid=name_get_uid)



class LibraryBookCategory(models.Model):
    _inherit = 'library.book.category'
    max_borrow_days = fields.Integer('Maximum borrow days', help="For how many days book can be borrowed",
                                     default=10)
